.ONESHELL:

SHELL = /bin/bash

ARCH := amd64
DISTFILES_URL := http://distfiles.gentoo.org
RELEASE_URL := $${DISTFILES_URL}/gentoo/releases/$${ARCH}/autobuilds

prepare:
	# prepare build environment

	set -e

	if [ -n "$$FLAVOR" ]; then export FLAVOR=-$$FLAVOR; fi
	export LATEST_FILE=$$RELEASE_URL/latest-stage3-$$ARCH$$FLAVOR.txt
	export LATEST=$$(curl -s $$LATEST_FILE | tail -n1 | awk '{ print $$1 }')
	export STAGE3_URL=$$RELEASE_URL/$$LATEST
	export STAGE3=$$(basename $$LATEST)

	# fetch files
	install -d ./build ./build/dl ./build/work
	wget $$STAGE3_URL -O ./build/dl/$$STAGE3
	wget $$STAGE3_URL.CONTENTS -O ./build/dl/$$STAGE3.CONTENTS
	wget $$STAGE3_URL.DIGESTS.asc -O ./build/dl/$$STAGE3.DIGESTS.asc

	# verify file legitimacy
	gpg --keyserver hkps.pool.sks-keyservers.net --revc-keys 0xBB572E0E2D182910
	gpg --verify ./build/dl/$$STAGE3.DIGESTS.asc
	sed -i '6d;7d;10d;11d' ./build/dl/$$STAGE3.DIGESTS.asc
	(cd ./build/dl && sha512sum --quiet --status -c $$STAGE3.DIGESTS.asc)
